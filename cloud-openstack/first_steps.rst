.. _cloud_openstack_first_steps:

First steps
===========

Before starting accessing to Openstack you should verify these aspects:

    1. **Have access to CESGA by a user account:**
By default, all HPC users with an active account have acess to the Cloud service. If you do not have an account and you are willing to use CLOUD services, you should request an account using `this link. <https://altausuarios.cesga.es/user/ualta>`_

    2. **Verified quota requirements:**
If you already have an account, you should check if the resources associated with the user accounts are sufficient to deploy the VM or VMs you have in mind. The maximum resources for the HPC user projects are: 

    - 4 instances.
    - 8 CPUs.
    - 8 GB of RAM memory. 
    - 4 volumes.
    - 12 snapshots of volumes.
    - 500 GB of disk storage. 
    - 1 floating public IP address.
    - 10 security groups.
    - 100 rules of security groups. 
    - 100 networks. 
    - 500 ports.
    - 10 routers.

If these resources turn out to be insufficient, you should contact Systems Department requesting the special resources you need with a brief explanation. 

You can access Horizon at `CESGA CLOUD <https://cloud.srv.cesga.es/auth/login/>`_ to check the quota limits associated with your project. The username and password are the same as the ones you used to access other CESGA services. The dominion is hpc. 


3. **If these resources are enough, your next steps to deploy a VM should be:** 

- Create or add your `public key. <https://cesga-docs.gitlab.io/cloud-openstack/keypair.html>`_
- Set the `security groups <https://cesga-docs.gitlab.io/cloud-openstack/security_groups.html>`_ for your VM.
- Create and add storage `volumes <https://cesga-docs.gitlab.io/cloud-openstack/volumes.html>`_ to the VM. 

4. **Once you have these configurations done, you can now** `deploy your VM <https://cesga-docs.gitlab.io/cloud-openstack/launch_vm.html>`_

.. Note:: If you are not familiarized with Openstack terms, you should take a look at the term's `glossary. <https://cesga-docs.gitlab.io/cloud-openstack/glossary.html>`_ 
