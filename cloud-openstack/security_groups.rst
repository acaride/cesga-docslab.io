.. _cloud_openstack_security_groups:

Security groups
===============
Security groups are sets of IP filter rules that are applied to the instances. They will define networking access to the VM. 

All projects have a **default security group** which is applied to any instance that has no other defined security group. Unless you change it, this security group **denies** all incoming traffic and **allows** only outgoing traffic to your instance.

Managing security groups
------------------------
If your MV has special networking access you must specifed it by the security groups. You will have to create different rules to grant incoming and/or outgoing traffic according to your needs. This step should be done before you lauch the VM but it's not mandatory, you can add more security groups to your VM even when the instance is running. 

To create these security groups follow this procedure:

1. Navigate through Network > Security groups > Create security group. 
2. Enter a name for the group and click on ``Create``. The new group appears in the list on the Security Group tab.
3. Configure the new rule by clicking on ``Manage Rules`` and selecting ``Add rule``. 
4. Once completed the rule definition and save the configuration, click ``Add``. 

We will be reviewing the most common security groups and rules. Keep in mind that each rule should be in a different security group and these are just some examples. 

- **SSH:**

.. figure:: _static/screenshots/ssh-access.png 
   :align: center

This rule will allow the owner of the machine or the users allowed by the `authorized_keys <https://cesga-docs.gitlab.io/cloud-openstack/keypair.html#adding-more-public-keys-to-authorized-keys>`_  to access the VM by SSH by the default port 22.  Most common rules are stored on the system, so, for this rule we just have to unfold the "Ruler" and select SSH. 

If you let the default settings as CIDR: 0.0.0.0/0 this will allow the connection to the VM by any IP. You can modify the IP range to only allow determined IPs if you want to add more security to your VM.


- **Custom TCP/UDP/ICMP rule:** 

.. figure:: _static/screenshots/tcp-rule.png 
   :align: center

You can add and open the incoming and outgoing traffic only to certain ports using TCP/UDP/ICMP rules: 

   * **Custom TCP Rule** is used to exchange data between systems and for end-user communication.
   * **Custom UDP Rule** is uUsed to exchange data between systems. 
   * **Custom ICMP Rule** is used by network devices, such as routers, to send error or monitoring messages.

Also, you can select a port, a range of ports, allow a IP or a range of IPs. 

- **MYSQL:**

.. figure:: _static/screenshots/mysql-port.png 
   :align: center

MYSQL uses 3306 as the default port. You can add it by a Custom rule but, as accesing to MYSQL is very often, there is a default rule created. You just have to select "MYSQL" at the "Ruler" section and it's not necessary to choose the port, it will be automatically selected by the system. Again, you can set the IPs allowed to connect to this port. 


.. Note:: If you have your `SSH keys <https://cesga-docs.gitlab.io/cloud-openstack/keypair.html>`_  and your security groups set, you can now `deploy your VM. <https://cesga-docs.gitlab.io/cloud-openstack/launch_vm.html>`_ 


Adding security groups to your VM
-------------------------------------
You can add your security groups when you are deploying your VM or you can add them later. 

For this second case, you must go to your instance, open the settings tab on the right menu and click on ``Edit security groups``. A new page will be opened where you could see your security groups. On the left are your security groups, to add them just click on the + symbol and save the changes. 

.. figure:: _static/screenshots/add-sg.png 
   :align: center

These rules will be automatically added to your VM, you don't have to reboot it. 


