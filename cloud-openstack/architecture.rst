.. _cloud_openstack_architecture:

Hardware architecture
=====================

Intel nodes
-----------

CESGA CLOUD is made up of **114 compute nodes** with the following characteristics: 

- Processors: 2x Intel Xeon Gold 6240R 2, 40GHz with 24 cores and 35'75MB cache each (48 cores per node)
- 12 DIMMs with 16GB each, total 192GB
- 2x480GB SSD of disk storage, total 960GB
- Network card: 1 Broadcom 57416 double port 10GbE

Also, there are **16 storage nodes** with the following characteristics: 

- Processors: 2x Intel Xeon Gold 6240R 2, 40GHz with 24 cores and 35'75MB cache each (48 cores per node)
- 12 DIMMs de 16GB, total 192GB
- 2 64GB SD on RAID 1
- 12 SAS disk with 16TB 
- 1 1'6TB disk NVMe Flash
- Network card: 1 Broadcom 57414 double port 10/25GbE


AMD nodes
----------

.. Warning:: These nodes are under installation so they are not available for use. 

**32 compute nodes** with the following characteristics: 

- Processors: 2x AMD EPYC 7443, 24 cores, 2.35 GHz (48 cores per node)
- 192GB of RAM memory
- 2x480GB SSD of disk storage configured on RAID 1, total 960GB
- Network card: 2 Broadcom 57416 double port 10GbE

There are also **4 compute nodes** with these special characteristics: 

- Processors: 2x AMD EPYC 7443 , 24 cores, 2.35 GHz (48 cores per node)
- 192GB of RAM memory.
- 2x960GB FLASH disk storage configured on RAID 1, total 1'6TB
- Network card: 2 Broadcom 57416 double port 100GbE

**8 storage nodes** with the following characteristics: 

- Processors: 2x AMD EPYC 7443 , 24 cores, 2.35 GHz (48 cores per node)
- 128GB of RAM memory
- 1 960 GB NVMe FLASH disk
- 2x 64 SD storage systems for operating system configured on RAID 1 
- Network card: 2 Broadcom 57414 double port 25GbE
- These nodes have access to a storage system with 12x16TB NL SAS disks
