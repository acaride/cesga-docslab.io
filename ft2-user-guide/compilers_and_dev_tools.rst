.. _ft2_compilers_and_dev_tools:

Compilers and development tools
===============================

La lista más actualizada de aplicaciones será la disponible mediante el
comando “\ ***module spider***\ ”. Inicialmente está disponible el
Intel® Parallel Studio XE 2016. El objetivo será tener disponible todas
las versiones consecutivas de esta suite. Así mismo están disponibles
los compiladores de GNU en diferentes versiones.

En cuanto al MPI, aparte del Intel MPI, estará disponible una versión
actualizada de OpenMPI.
