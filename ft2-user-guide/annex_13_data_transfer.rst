.. _ft2_annex_13_data_transfer:

Annex XIII: Data transfer
=========================

El *Data Transfer Node (DTN)* es un servidor configurado especialmente
para llevar a cabo transferencias más rápidas y fiables. Este nodo
proporciona diferentes herramientas para transferir archivos desde o
hacia el CESGA. Dependiendo del tamaño de los datos que desea
transferir, tiene diferentes opciones:

-  Para pequeñas cantidades de datos ( <10 GB) se pueden usar comandos
   como ***scp*** o ***rsync*** directamente.
-  Para grandes cantidades de datos, se recomienda el uso de
   ***Globus***.

En ambos casos, se recomienda que se realice la transferencia contra
nuestro servidor DTN: ***dtn.srv.cesga.es***, el cual tiene un mejor
rendimiento de red en comparación con los otros nodos de login del Finis
Terrae II. En Globus Online, el punto final de este servidor es:
***cesga#dtn***.

Esta última opción, le permitirá la compartición de datos con personas
que no tengan acceso al CESGA.

Puede encontrar toda la documentación en:

`*https://bigdata.cesga.es/dtn-user-guide/CESGA\_DTN\_User\_Guide.pdf* <https://bigdata.cesga.es/dtn-user-guide/CESGA_DTN_User_Guide.pdf>`__

*Nota.- Este nodo es similar a los nodos de login, por lo que podría
usarse como tal. Pero dado su uso especial para la transferencia de
datos, debe ser utilizado solo para cosas puntuales que no interfieran
con esas posibles transferencias.*
