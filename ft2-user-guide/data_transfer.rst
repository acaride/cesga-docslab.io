.. _ft2_data_transfer:

Data transfer
=============

For files transfer to or from the Finis Terrae II an scp or sftp client must be used.

