.. _ft2_annex_10_cron:

Annex X: Redundant cron
=======================

Cuando te conectas al Finis Terrae II, accedes a uno de los 4 nodos de
login disponibles. Normalmente, cada vez que te conectes, accederás al
mismo nodo de login pero esto puede cambiar, por ejemplo, si accedes
desde sitios diferentes (desde tu centro y desde casa), o si el nodo al
que solías acceder no está disponible en ese momento.

Supongamos que necesitas configurar una tarea en el cron para que se
ejecute cada cierto tiempo. En este caso, tendrías que editar tu cron
con el comando “\ ***crontab -e***\ ” y adecuarlo a tus necesidades. Una
vez hecho esto, tu tarea se ejecutará en el momento indicado en el nodo
de login en el cual lo hayas configurado.

*Nota.- El acceso al cron está restringido por lo que aquellos usuarios
que quieran utilizarlo deberán solicitarlo enviando un mail a
*\ `*sistemas@cesga.es* <mailto:sistemas@cesga.es>`__\ *.*

Si la siguiente vez que te conectes al Finis Terrae II, el nodo de login
asignado es diferente al cual en el que habías configurado el cron te
encontrarás que este no contiene las tareas que esperabas, lo que te
puede llevar a pensar que se perdió tu configuración y volver a
añadirlas en este nuevo nodo de login y por lo tanto, esas tareas se
estarían ejecutando dos veces en diferentes nodos de login. Para evitar
este tipo de situaciones o que tengas que acordarte en qué nodo de login
habías configurado el cron y que tengas que acceder a él cada vez que
quieras hacer alguna gestión en el cron, hemos preparado los nodos de
login para que todos ellos puedan gestionar el mismo cron como si se
tratase de un único nodo. De este modo, todos los nodos de login tendrán
una copia de tu cron pero solo se ejecutará en uno de ellos.

Otra ventaja del cron redundante, es que aunque se caiga uno de los
nodos frontales, tu cron se ejecutará sin problemas, mientras que si no
se usa y el nodo que no funciona es en el que tenías establecido el
cron, las tareas ahí configuradas no se ejecutarán con los consiguientes
problemas.

Para utilizar el cron redundante deberás seguir los siguientes pasos:

1. Añadir las siguientes líneas al inicio del cron:

***SHELL=/bin/bash***

***PATH=/sbin:/usr/sbin:/usr/local/sbin:/bin:/usr/bin:/usr/local/bin:***

*Nota.- En el caso de que ya tengas definida la variable PATH en tu
cron, deberás asegurarte de que esté incluido el directorio
**/usr/local/bin** en dicha variable.*

1. Añadir “\ ***run***\ ” delante de cada comando a ejecutar en el cron.
   Por ejemplo, si tenemos una línea como esta:

***15 12 \* \* \* comando***

Tendrías que modificarla para que quedase así:

***15 12 \* \* \* **\ **run**\ ** **\ **comando***

1. Tras haber editado y guardado el cron, se deberá ejecutar el comando
   “\ ***sync\_cron***\ ” para sincronizar este nuevo cron con todos los
   nodos de login.

*Nota.- Es muy importante acordarse de ejecutar este comando cada vez
que se haga un cambio en el cron pues de no hacerlo, se podrían perder
los cambios hechos o que no se ejecuten las tareas indicadas porque el
nodo que tenga que hacerlo no tenga la configuración adecuada.*

*Nota.- Se debe tener en cuenta, que estas tareas serán ejecutadas en un
nodo de login, por lo que no deberán ser tareas costosas en cuanto al
consumo de recursos.*
