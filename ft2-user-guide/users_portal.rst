.. _ft2_users_portal:

Users Portal
==============

A web portal is available for CESGA users through the
following link:

`https://portalusuarios.cesga.es <https://portalusuarios.cesga.es>`_

Where the following information can be found:

- List of queued jobs. From the portal you can make basic actions on these jobs such as canceling or viewing the status
- List of jobs completed by users in the last few days
- Space occupied by the user on disk
- Information on the status of the calculation systems, CPU consumption or storage used in the last year
- Information on how to use the systems: job submission, available storage, installed applications, how to connect etc...
- How to get support for the most common problems or open a support case (ticket)