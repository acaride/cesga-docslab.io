To create the documentation run:

    load_anaconda2
    make clean
    make html
    make latexpdf

You can use the build script:

    load_anaconda2
    ./build.sh

Finally you can sync the contents with a public web server:

    ./sync_web.sh


## Software required

You will need an Anaconda2 installation.

Then you have to install the sphinx theme:

    pip install sphinx_rtd_theme

To build the pdfs in Ubuntu you will need:

    apt install texlive texlive-latex-recommended texlive-latex-extra latexmk
