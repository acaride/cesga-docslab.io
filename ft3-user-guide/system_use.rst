.. _ft3_system_use:

System use
===========

There are four ways to use the equipment, depending on the type of resources and interactivity that are needed:

- **Interactive use:** when connecting to the computer you have access to limited and shared resources, invalid to perform simulations, but fine to work with files and interact with the system.  The maximum virtual memory available is 8GB.

- `Remote Desktops: <https://cesga-docs.gitlab.io/ft3-user-guide/remote_desktops.html>`_  for remote desktop access you can visit `this URL <https://portalusuarios.cesga.es/tools/remote_vis>`_ through which you have access to a complete graphic X11 system to display and access those applications that have a graphical interface. However, it will not be possible perform long simulations or use more than 32GB of memory.

- **Dedicated interactive nodes:** it is possible to start interactive sessions but with dedicated resources using the command ``compute`` (for more information it can be used with the option ``--help``) from the node the user connects to. Still, the maximum resources available are 64 cores and 247GB of RAM memory up to 8 hours.

- **Using the queuing system:** to perform simulations with multiple cores and large amounts of memory and time, it is necessary to use the queuing system, which guarantees the resources assigned to each user.
