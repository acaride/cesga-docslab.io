.. _ft3_remote_desktops:

Remote desktops
===============

Remote desktops are a tool for accessing FinisTerrae III under a visualization mode that is easier to use for some users. You can create them at the User Portal > Tools > `Remote visualization FT-III. <https://portalusuarios.cesga.es/tools/remote_vis>`_ 
Once you create a desktop, it will be alive for **36 hours**. After that, if you didn't extend the time for its use, the desktop will close automatically and any information not saved will be lost. 

* On the top bar, you can see the remaining time for the desktop to close and if you click there, you can extend your desktop session for 36 more hours. Note that the change will be visible after **2 minutes** from the extension, it is not immediately. 

* If you slide the mouse on the following circular graphic icon you will be able to see your Home `quota limits <https://cesga-docs.gitlab.io/ft3-user-guide/storage.html>`_ , both those for files and storage space. 

* The screen icon is meant to change the **resolution** of the remote desktop ir orden to suit it to the users local screen. 

* Terminal icon will open a **new terminal window** at user's Home directory of the FinisTerrae III. 

* Firefox icon will launch the website on a new window. 

* **Clipboard**: is the left panel used to comunicate your local PC with the remote desktop. It provides a keyboard, a proper clipboard meant to copy and paste between your local PC and the remote desktop, full-screen mode, settings and a disconnection icon. 
 
.. warning:: If you are trying to open a Remote Desktop but it crashes from both User Portal or SSH sites, you should check your **$HOME quota**. If you are exceeded the quota limits or you are close to trespassing it, you won't be allowed to create the Remote Desktop. To avoid this problem, free some space at your $HOME directory by deleting data you don't need or to moving it to your $STORE or $LUSTRE directories. For storage and quota information visit: `Permanent Storage. <https://cesga-docs.gitlab.io/ft3-user-guide/storage.html>`_ 


Desktops CLI
------------

There is also a desktops CLI that you can use. It is already configured with bash autocompletion, here are some examples:

* ``desktops``: allows to manage your remote desktop from the command line.

* ``desktops create``: it will create a new desktop and return the URL.

* ``desktops delete``: it will erase the desktop. It's a way to kill the desktop if, for any reason, the option available to delete the desktop is not working on the User Portal. 

* ``desktops extend``: it will extend the time for the remote desktop for 36 hours. 

* ``desktops --help``: it shows more information about the use of the command. 
