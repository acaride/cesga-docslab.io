.. _ft3_cron:

Redundant cron 
==============

When you connect to FinisTerrae III, you access one of the 4 available login nodes. Normally, each time you connect, you will access the same login node but this can change, fif you access from different places (from your center or from home), or if the node you used to access is not available at that moment.

Suppose you need to configure a cron job to run ofenly. In this case, you would have to edit your cron with the command ``crontab -e`` and adapt it to your needs. Once this is done, your task will be executed at the indicated time in the login node in which you have configured it.

.. note:: The use of the cron is restricted so you must made a request to `sistemas@cesga.es <mailto:sistemas@cesga.es>`_ to get access.

If the next time you connect to FinisTerrae III the assigned login node is different from the one in which you had configured the cron, you will find that it **does not** contain the tasks you expected. This may lead you to think that your cron has been lost and you will probably confgigurate the cron again in this node and therefore those tasks would be running twice in different login nodes. 

To avoid this type of situation you will have to remember in which login node you had configured the cron and that you have to access it every time you want to do some cron management. All login nodes are prepared so all of them can manage the same cron as if it were a single node. The login nodes will have a copy of your cron but it will only run on one of them.

To use the redundant cron you must follow the following steps:

1. Add this lines in the beggining of the cron::

    SHELL=/bin/bash
    PATH=/sbin:/usr/sbin:/usr/local/sbin:/bin:/usr/bin:/usr/local/bin:

In case you already have the ATH variable defined in your cron, you must make sure that the directory */usr/local/bin** is included in thath variable.

2. Add ``run`` before each command the cron has to execute::

    15 12 run <command>

3. After editing and saving the cron, the ``sync_cron`` command must be executed to synchronize this new cron with all login nodes.

.. note:: It is very important to remember to execute this command every time a change is made in the cron. If not, the changes made could be lost or the indicated tasks may not be executed because the node has not the proper configuration. It also must be taken into account that these tasks will be executed in a login node, so they should not be expensive tasks in terms of resource consumption.
