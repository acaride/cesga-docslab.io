.. _ft3_batch_binding:

Binding: tasks on specific cores
================================
In the current multicore architectures, to obtain an optimal performance it is always recommended to use options for "*binding*" processes to physical CPUs (cores), not allowing the system to migrate processes between the different cores and ensuring the closest proximity between the data in the memory and the core that reads/writes them. 
By default in the FinisTerrae III configuration each of the requested tasks are associated with a cgroup (Linux Control Groups) that will not allow the task's processes to run outside of the physical resources associated with that task.

