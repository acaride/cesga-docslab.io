.. _ft3_how_to_connect:

How to connect
==============
As explained at `Firsts steps <https://cesga-docs.gitlab.io/ft3-user-guide/first_steps.html>`_ if you are accesing to the CESGA's resources from an authorized center you don't need to configure the VPN.

Another way to access to our servers without using the VPN is via `User Portal <https://portalusuarios.cesga.es/>`_ 
with your username and password. In Tools head, you have an `SSH Terminal <https://portalusuarios.cesga.es/info/web_ssh>`_ that opens a command terminal directly at FinisTerrae III. The other option are
`Remote Desktops, <https://portalusuarios.cesga.es/tools/remote_vis>`_ they provide a Linux desktop with direct access to the directories and command terminal of the FinisTerrae III. This Remote desktops will be destroyed after 36 hours unless you log out of the desktop before reaching that limit or restarting the countdown.
If you want more information about the use of the Remote Desktops you can check `this page. <https://cesga-docs.gitlab.io/ft3-user-guide/remote_desktops.html>`_

If you are not at your center or you are working abroad (at your house or even in another country) you must use the VPN to be able to connect with our servers (except from SSH Terminal and Remote Desktops as explained above). Enabling a VPN connection is mandatory in all these cases and the VPN must be establish using **FortiClient**. The installation and configuration of this tool is discussed below for different operating systems.

Installation and configuration of FortiClient on Windows and MacOS
------------------------------------------------------------------

.. warning:: If the client freezes at 40%, it is due to a bug in FortiClient version 7.0 or later. To fix this error, we recommend downloading and installing version **6.2** for `Windows <https://cumulo.cesga.es/index.php/s/AFwnC7SSDzesFqg>`_ and `MacOS <https://cumulo.cesga.es/index.php/s/XDXm7nMW4qoS5rb>`_ .


Install FortiClient as VPN client provider which can be downloaded from their website. We will configure the VPN connection as follows (some differences between the Windows client and MacOS client could appear but the parameters of configuration are the same):

.. figure:: _static/screenshots/vpn1.png 
   :align: center

For its configuration it is necessary to establish the VPN name as **gateway.cesga.es**. You have to **check the Customize port box** (with the number **443**) since it is not checked by default even if you write the number. 

It's important to underline that the **username** is the **complete email** with which the user **requested the registration** to access our servers. The domain may change between institutions, universities and other associated centers. If you have doubts about which is your registration email, you can check it in the `User Portal Profile. <https://portalusuarios.cesga.es/user/perfil>`_ 

.. note::
   
   For the users with a "curso" account, the login will be cursoNNN@cesga.es being NNN the number given to that account. For example: curso101@cesga.es
   
Installation and configuration of VPN-Fortissl on Linux
-------------------------------------------------------

**1. Download it** `here <https://portalusuarios.cesga.es/layout/download/vpn-fortissl.rar>`_ **and follow these steps as root:**

.. code-block:: console

    unrar e vpn-fortissl.rar
    tar xvzf forticlientsslvpn_linux_4.4.2323.tar.gz
    cd forticlientsslvpn
    ./fortisslvpn.sh
    Accept the license agreement presented
    ../forticlientsslvpn_cli --server gateway.cesga.es:443 --vpnuser usuario@dominio.com

Alternatively if you have a Linux OS (Debian, Ubuntu or Mint), you can install **network-manager-fortisslvpn**. This package belongs to the 'universe' repository which is normally enabled by default. 

On Linux systems, to create the VPN from the GNOME network manager, it is also necessary to install the network-manager-fortisslvpn-gnome package. Without that package the "VPN" tab does not appear within the fortisslvpn configuration.

**2. Configure the connection as root as follows:**

.. code-block:: console

    Gateway Remoto: gateway.cesga.es
    Port: 443
    Username: email@dominio.com (it is the one used in the user registration)
    Password: your_password

Installation and configuration of OpenFortiVPN on Linux
-------------------------------------------------------
There is also an alternative open-source client called `OpenFortiVPN <https://github.com/adrienverge/openfortivpn>`_ that you can use instead of FortisslVPN. Some Linux distibutions like Ubuntu, Debian, OpenSuse or Arch Linux provide OpenFortiVPN packages.

- Ubuntu 18.04 or newer versions: 

.. code-block:: console

    sudo apt install openfortivpn

- Integration with the NetworkManager in GNOME:

.. code-block:: console

    sudo apt install network-manager-fortisslvpn-gnome

-  Centos 7 ( available in the EPEL repo):

.. code-block:: console

    yum install openfortivpn

**2. For any of the cases above, configure the connection as root as follows:**

.. code-block:: console

    Gateway Remoto: gateway.cesga.es
    Port: 443
    Username: email@dominio.com (it is the one used in the user registration)
    Password: your_password
    
    No certificated are need to stablish the connection. 

Once openfortivpn is installed you can start the VPN executing 
``sudo openfortivpn gateway.cesga.es:443 -u username@dominio.com -p "your_password"``. Once you submit that command, the window will show the next information, the window **will become blocked** and you can no longer use it. You will have to open a new window in your command terminal. 

.. code-block:: console

    VPN account password:
    INFO: Connected to gateway.
    INFO: Authenticated.
    INFO: Remote gateway has allocated a VPN.
    WARN: No gateway address, using interface for routing
    Using interface ppp0
    Connect: ppp0 <--> /dev/pts/4
    INFO: Got addresses: [...], ns [0.0.0.0, 0.0.0.0]
    INFO: Negotiation complete.
    INFO: Negotiation complete.
    local IP address ...
    remote IP address ...
    INFO: Interface ppp0 is UP.
    INFO: Setting new routes...
    INFO: Adding VPN nameservers...
    INFO: Tunnel is up and running.

In case you don't want the window to block you can edit /etc/openfortivpn/config with the following information and execute ``sudo openfortivpn &``

.. code-block:: console

    # config file for openfortivpn, see man openfortivpn(1)
    host = gateway.cesga.es
    port = 443
    username: email@dominio.com (it is the one used in the user registration)
    password: your_password
 
You can also create an alias (activate_VPN) to start OpenFortiVPN:

.. code-block:: console
    
    $ cat activate_vpn
    
    VERSION=$(lsb_release -r -s)

    if [[ $VERSION == "20.04" ]]; then
        sudo wg-quick up wg0
    else
        sudo openfortivpn &
    end


.. Note:: Finis Terrae III: ft3.cesga.es (Fingerprint SSH: SHA256:LeEPzn5dC89HQ/54mnSKnAqam/cLNiiNqZS/MosZ7VY)

Remote connection
-----------------
If you are using Linux or MacOS as your operating system, you can access to Finisterrae III directly from the command terminal but if you are a Windows user, you will need to install a UNIX like environment.

.. Note::
    New versions of Windows allow to make the connection via SSH with the Windows command terminal (cmd) so is not mandatory to use a UNIX client

Anyway, **MobaXterm** is a tool that will provide a complete environment for connecting to the Finisterrae III and transferring files local and to Finisterrae III. You can download and install MobaXterm from its official page. Once installed, the first step would be to create a new user session, configuring it as follows:

.. code-block:: console

    Remote Host: ft3.cesga.es
    Specify username: username (Just the username without @)
    Port: 22

Next, a new tab will open in the terminal, in which you will be asked for the password of your account. For security, when the password is typed the cursor does not move nor shows any type but the password is being written.

Once the password is verified, it will show you the Finisterrae III home screen and you will be in one of the login nodes, from which you can edit and copy/move files and submit jobs to the queue system. If not, you just have to enter using the following address:

.. code-block:: console

    ssh username@ft3.cesga.es

