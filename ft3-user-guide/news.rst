.. _ft3_news:

What's new at CESGA?
====================

March 1, 2023: AMD EPYC Nodes
--------------------------

We have add 18 nodes with 2x AMD EPYC 7452 @ 2.35GHz with 32 cores each (64 cores per node), 256GB of RAM memory and 2TB HDD of local storage.

To use these nodes you have to add the option ``-C epyc`` when you submit a job with the ``sbatch`` command. Example::

    $ sbatch -C epyc -t 24:00:00 --mem=4GB script.sh

.. warning:: If you are using Intel libreries in your jobs, they could fail in these AMD nodes as long as they are not Intel supported. Some libraries can work on AMD nodes but others not, causing a failure in your jobs. 


December 2, 2022: Intel Cascade Lake nodes 
-------------------------------------------

There are 94 nodes with 2x Intel Xeon Gold 6240R (Cascade Lake) with 24 cores each (48 cores per node), 180GB of RAM memory and 2x480GB SSD of local storage. There are also known as clk nodes. **20 of these nodes have special priority so they are not available all the time for general use.**

To use these nodes you have to add the option ``-C clk`` when you submit a job with the ``sbatch`` command. Example::

    $ sbatch -C clk -t 24:00:00 --mem=4GB script.sh

These nodes, since they are not connected via the high-performance Mellanox Infiniband interconnect network, access to LUSTRE directories has lower performance. So if your jobs are I/O intensive in LUSTRE, they may be affected on these nodes. 
