.. _ft3_envs_modules:

Environment modules (Lmod)
==========================

.. |module_av_1| image:: _static/screenshots/module_av_1.png
    
.. |module_av_2| image:: _static/screenshots/module_av_2.png
    
.. |module_av_3| image:: _static/screenshots/module_av_3.png

``module`` command is the central command that allows the configuration 
and use of the applications provided by CESGA. It can be used as module or in its short form ml. It sets the appropriate environment variables for a given application independent of the user shell. The available applications are hierarchical and are tied to the 
preload of a particular combination of dependencies:

- compiler: with which the application was compiled. 
- MPI: for MPI applications, the version of MPI used in its compilation. 

This tries to minimize incompatibility problems between the different 
combination of compiler/MPI libraries.

.. Warning:: We are having some issues listing and loading modules using spider. The error message is: "/usr/bin/lua: /usr/share/lmod/lmod/libexec/FrameStk.lua:125: attempt to index a boolean value (local 'mname')".
  To fix this you can try loading the module with the following command:  ``module --ignore-cache spider``.  If this does not resolve the issue, you may need to delete the .lmod directory located in your $HOME: ``rm -rf $HOME/.lmod.d``

Available modules
-----------------

.. code-block:: console

  module avail (short form: ml av)

|module_av_1|

...

|module_av_2|

...

|module_av_3|

Initially, the only modules available are those belonging to the "Core" 
section where compilers and applications whose use is independent of 
the compiler/MPI combination are located (applications compiled with the 
default gcc/glibc version used as the basis for the rest of the 
compilers and therefore compatible with each other).

In the “ORGS” section, the organizations appear (trees of independent 
modules to which the user has access). By default the available 
organizations are:

+--------------+----------------------------------------+
| **ORGS**     | **Compiler/default glibc**             |
+--------------+----------------------------------------+
| cesga/2020   | Gentoo Prefix: GCC 10.1.0 glibc 2.31   |
+--------------+----------------------------------------+

Cesga/2020 contains the following packages: 
  - gcc/system
  - openmpi/4.0.5
  - gcccore/system
  - impi/2021.2.0
  - impi/2021.3.0
  - openmpi/4.1.1_ft3
  - intel/2021.3.0
  - intel/2021.2.0

Applications
------------
Applications are grouped into the following areas:

-  Comp:Compiler (key: **area_compiler**)
-  MPI:MPI library (key: **area_mpi**)
-  Bio:Bioinformatics (key: **area_bioinformatics**)
-  Chem: Chemistry and Materials (key: **area_chemistryandmaterials**)
-  Ed: Editor (key: **area_editor**)
-  ML: Machine Learning (key: **area_machinelearning**)
-  MPCFD: Multiphysics and CFD (key: **area_multiphysicsandcfd**)
-  Math: Math Library (key: **area_mathlibrary**)
-  Prof: Profiling (key: **area_profiling**)
-  ScA:Scientific Analysis (key: **area_scientificanalysis**)
-  Sim:Simulation (key: **area_simulation**)
-  Soft: Software Management (key: **area_softwaremanagement**)
-  Tool: Tool (key: **area_tools**)
-  VisF: Visualization and data formats (key: **area_visualizationanddataformats**)

Using the key associated with each area, it is possible to search for 
the applications grouped in it using the `key` option of the ``module`` 
command. The full list of applications available can be found `here. <https://www.cesga.es/en/infrastructures/applications/>`_

Module load use
-----------------
Load a module to configure the environment for an application use::

$ module load intel

When loading the module corresponding to the intel compilers, the 
applications section corresponding to these compilers is available for l
oading. The available MPI modules appear. If an MPI module is loaded, 
for example impi, the available applications section for the 
compilers (intel) / MPI (impi) combination becomes available.

Unload module/s::

$ module unload package1 package2 …

Unload all loaded modules and reset everything to original state::

$ module purge

Currently loaded modules::

$ module list

It may be interesting to change the compilers in use::

$ module swap intel gcc

With this command all compiler dependent libraries/applications will be 
changed. If the applications for the compiler do not exist, the 
corresponding modules are marked as inactive, disabling their use until 
the switch back to the original compiler is made.

The modules contain brief use guides of the corresponding 
applications/libraries. These are obtained by the command::

$ module help packageName

In order to obtain help for a particular module using this command, it 
must be listed using the ``module avail`` command (hierarchy 
dependencies loaded).

For help on using the module command itself::

$ module help

To see all available applications/libraries regardless of the 
compilers/MPI they depend on, use the command::

$ module spider

Without any arguments this command returns the complete list of 
available applications/libraries. It is possible to search using this command if an application is available.

