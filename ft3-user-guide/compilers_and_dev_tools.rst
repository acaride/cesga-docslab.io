.. _ft3_compilers_and_dev_tools:

Applications, compilers and development tools
=============================================

**- Appications**: CESGA provides a wide variety of pre-built applications, optimized for 
Finisterrae III. The primary way these are provided in the session 
environment is through modules. Please use the command ``module spider``
to see the complete list of available applications.

**- Compilers**: Initially the recommended compilers for the C, C++, and Fortran 
languages are the included on the `Intel® oneAPI HPC toolbox 2021.3 <https://www.intel.com/content/www/us/en/developer/tools/oneapi/hpc-toolkit.html>`_.
This suite will be updated as new versions will be released.
Also GNU compilers are available in different versions.

**- MPI**: Intel MPI is included in the `Intel® oneAPI HPC toolbox 2021.3 <https://www.intel.com/content/www/us/en/developer/tools/oneapi/hpc-toolkit.html>`_. 
Different OpenMPI versions (started from 4.0.5 version) are also available.
