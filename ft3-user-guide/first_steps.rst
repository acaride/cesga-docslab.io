.. _ft3_first_steps:

First steps
===================

Creating an account
--------------------

The first step is to `register as user. <https://altausuarios.cesga.es/user/ualta>`_ We highly recommed you to follow the steps listed on the website and check out the user registration's diagram in order to avoid any issues with your application. 

If you are a member of CSIC and you want to create a group you have to request a `group registration <https://altausuarios.cesga.es/user/galta>`_ and follow the steps. 

Connecting with CESGA 
----------------------
For security reasons, access to our servers is restricted to authorized centers (Galician universities,  CSIC centers, centers with special agreements ...), so access to them is only possible from these registered centers and users. Out of these hubs, the connection must be made using VPN. 

Access to our servers is done using an SSH (Secure Shell) client that allows encrypted information transfers. SSH allows the connection between computers over the network, executing commands on the remote machine and moving files from one machine to another. Provides strong authentication and secure communications over non-secure channels. All communications are automatically and transparently encrypted, including passwords. 

This prevents password capture, one of the most common means by which computer system security is compromised. Most versions of SSH provide a remote copy (SCP) operation, and many also provide a secure ftp (SFTP) client. Additionally, SSH allows secure X-Windows connections.

For more information about the VPN and how to configure it follow the steps at `how to connect. <https://cesga-docs.gitlab.io/ft3-user-guide/how_to_connect.html#configure-vpn>`_

.. note::
    This `authorization request <https://altausuarios.cesga.es/solic/conex>`_ is intended for new center asociations or for users which need mandatory the registration of their public IP to granteed access due to technical problems.

