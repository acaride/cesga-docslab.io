.. _ft3_fat_node:

High Memory nodes (fat nodes)
=============================
These nodes will be assigned automatically by the queue system if you required more than 100GB of RAM by core or if you request more RAM than a basic *ilk node* (247GB) or a **clk node** (180 GB). 
 
- `smp nodes: <https://cesga-docs.gitlab.io/ft3-user-guide/overview.html#node-groups-description>`_ 16 nodes with 64 cores and 2011GB of RAM available. 

- `Optane: <https://cesga-docs.gitlab.io/ft3-user-guide/overview.html#node-groups-description>`_ 1 node with 64 cores and 7975GB of Optane memory available. It should be taken into account that the performance of this memory is lower than the RAM memory, so execution time in this node can be greater. 

The smp nodes are avilable for all users but Optane node use is restricted. To get access to this node  in case you wish to use it, you must send an email to `sistemas@cesga.es <mailto:sistemas@cesga.es>`_


