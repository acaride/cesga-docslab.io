.. CESGA FT 3 User's Guide documentation master file

FinisTerrae III User Guide
==========================

.. |ft3| image:: _static/screenshots/ft3.png
    :width: 320px

.. |federft3| image:: _static/screenshots/Cartel-FEDER-FTIII-800x571.png
    :width: 320px
    

+---------------------------+
| |ft3|       |federft3|    |
+---------------------------+   

**User Guide's Index**

.. toctree::

   news
   overview
   first_steps
   glossary
   how_to_connect
   system_use
   remote_desktops
   data_transfer
   storage
   scratch_dirs
   parallelization
   batch_system
   binding
   cron
   job_signals
   job_requeue
   compilers_and_dev_tools
   env_modules
   fat_nodes
   gpu_nodes
   other_nodes
   FAQ
   want_to_know_more

