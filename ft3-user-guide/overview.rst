.. _ft3_overview:

Overview
========

FinisTerrae III with a total computing power of 4,36 PetaFLOPS is made up of 357 nodes interconnected via an Infiniband HDR network. There are 714 latest generation Intel Xeon Ice Lake 8352Y processors with 32 cores at 2.2Ghz (22.848 cores) and 157 GPUs (141 Nvidia A100 and 16 Nvidia T4).

.. figure:: _static/screenshots/FT3_schema.jpg 
   :align: center


Node groups description
-----------------------

**256 ilk compute nodes:** 16.384 cores 
   * 2x Intel Xenon Ice Lake 83252Y with 32 cores each (64 cores per node)
   * 256GB of RAM memory (247GB for real use)
   * 960GB SSD NVMe of local storage
   * 1 Infiniband HDR 100 connection 

**64 a100 compute nodes with GPU accelerators:** 4.096 cores
   * 2x Intel Xenon Ice Lake 83252Y with 32 cores each
   * 256GB of RAM memory (247GB for real use)
   * 960GB SSD NVMe of local storage
   * `2x NVIDIA A100 GPUs <https://cesga-docs.gitlab.io/ft3-user-guide/gpu_nodes.html>`_. 
   * 1 Infiniband HDR 100 connection 

**1 a100 compute node with 5 GPU accelerators:** 64 cores
   * 2x Intel Xenon Ice Lake 83252Y with 32 cores each 
   * 256GB of RAM memory (247GB for real use)
   * 960GB SSD NVMe of local storage
   * `5x NVIDIA A100 GPUs <https://cesga-docs.gitlab.io/ft3-user-guide/gpu_nodes.html>`_. 
   * 1 Infiniband HDR 100 connection 

**1 a100 compute node with 8 GPU accelerators:** 64 cores
   * 2x Intel Xenon Ice Lake 83252Y with 32 cores each 
   * 256GB of RAM memory (247GB for real use)
   * 960GB SSD NVMe of local storage
   * `8x NVIDIA A100 GPUs <https://cesga-docs.gitlab.io/ft3-user-guide/gpu_nodes.html>`_. 
   * 1 Infiniband HDR 100 connection 

**16 T4 compute nodes with GPU accelerators:** 1.024 cores
   * 2x Intel Xenon Ice Lake 83252Y with 32 cores each 
   * 256GB of RAM memory (247GB for real use)
   * 960GB SSD NVMe of local storage
   * `1x NVIDIA T4 GPU <https://cesga-docs.gitlab.io/ft3-user-guide/gpu_nodes.html#tesla-t4>`_. 
   * 1 Infiniband HDR 100 connection 

**2 dtn data nodes:** 128 cores
   * 2x Intel Xenon Ice Lake 83252Y with 32 cores each
   * 256GB of RAM memory (247GB for real use)
   * 960GB SSD NVMe of local storage
   * 1 Infiniband HDR 100 connection 
   * There is more information of these nodes at `Data transfer. <https://cesga-docs.gitlab.io/ft3-user-guide/data_transfer.html#dtn-nodes>`_ 
**16 smp nodes:** 1.024 cores 
   * 2x Intel Xenon Ice Lake 83252Y with 32 cores each 
   * 2TB of RAM memory (2011GB for real use)
   * 1920GB SSD NVMe of local storage
   * 1 Infiniband HDR 100 connection   
   * More info about `Fat Nodes <https://cesga-docs.gitlab.io/ft3-user-guide/fat_nodes.html>`_ 

**1 smp node (Optane):** 64 cores
   * 2x Intel Xenon Ice Lake 83252Y with 32 cores each 
   * 8TB of RAM memory (7975GB for real use)
   * 1920GB SSD NVMe of local storage
   * 1 Infiniband HDR 100 connection  
   * More info about `Fat Nodes <https://cesga-docs.gitlab.io/ft3-user-guide/fat_nodes.html>`_ 

In addition to these nodes, a series of nodes recovered from previous clusters are available:

**94 clk compute nodes:** 4.512 cores
   * 2 Intel Xeon Gold 6240R (Cascade Lake) with 24 cores (48 cores per node)
   * 192GB of RAM memory (180GB for real use)
   * 2x 480GB SSD of local storage
   * 10 Gigabit Ethernet connection
   * **20 of these nodes have special priority so they are not available all the time for general use**

.. warning:: MPI jobs using multiple nodes are not allowed in this partition since Infiniband is not supported.
