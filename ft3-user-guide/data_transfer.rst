.. _ft3_data_transfer:

Data transfer
=============
To transfer files from or to the FinisTerrae III is necessary a *scp* or *sftp* client or using ``rsync`` command. There are two special nodes for bulk data transfer known as **dtn-1** and **dtn-2**.These nodes are meant to be used for large data transfering (GB or even TB). There is more information about this nodes down below. 

.. warning:: LUSTRE directories of FinisTerrae II and FinisTerrae III are **different** systems so you have to copy the desired data from FinisTerrae II to FinisTerrae III. The shutdown of FinisTerrae II was planned for september 2022. After this date its operation cannot be guaranteed, especially the data stored in its LUSTRE. Therefore, all users are advised to migrate their files to FinisTerrae III's LUSTRE, as indicated below.


MobaXterm
---------

As explained on `how to connect, <https://cesga-docs.gitlab.io/ft3-user-guide/how_to_connect.html>`_ **MobaXterm** is a tool that can provide both SSH access and a file transfering environment. Its interface makes easy data transfer avoiding using commands especially for new users. The basic mechanism is to go to the FinisTerrae III directory to which you want to move the files in the left panel of the program interface. The next step will simply be to drag the files from your local computer folder to that left panel of the console. 

The same methodology can be applied to transfer files from FinisTerrae III to your local computer just dragging the icon file. 

SCP and SFTP clients
--------------------
**WinSCP** is a tool that provides an environment for data transfering. At first, you have to configure the conection to FinisTerrae III as follow: 

.. code-block:: console

        Protocol: SFTP
        Name or server IP: ft3.cesga.es
        Port: 22
        Username: username (just the name, not the full email)
        Password: your_password

Once connected, you will have your local computer on the left and FinisTerrae III directories on the right. The data transfer is also dragging files for one directory to other. You can also navegate between directories on both sides. 
There are more SCP and SFTP clients which can be used, these two are just examples.

DTN nodes
---------

As explained aboved, DTN nodes are meant to be used for big data transfers. **These nodes are exclusively for data transfer. Any type of interaction not related to data transferring will be eliminated and the user could be penalized.** There are two ways of accessing them based on being a CESGA user or not.

- **Access to the DTN nodes for CESGA users:**

Access to these nodes is equal to FinisTerrae-III login nodes. DTN nodes can be accessed by the hostname ``dtn.srv.cesga.es``
User and password are the same for FinisTerrae III. You can use ``rsync`` or transfer commands to start the data transfer and they can also be directly accessed via sftp with the following command: ``sftp username@dtn.srv.cesga.es``

There is also the possibility to use WinSCP, Filezilla or similar clients to perform the data tranfer using the following configuration:

.. code-block:: console

        Hostname: dtn.srv.cesga.es
        Port: 22
        Username: username 
        Password: your_password

Also, ASPERA is available in these nodes for those users who want or can use it as another way to perform the transfer.

.. Warning:: VPN access to these nodes is NOT POSSIBLE, so data transfers must be made from the authorized work center of the user. Exceptionally, if a user is outside these centers, we can add the public IP to give them access. If this is your case, you must contact the Systems Department indicating the data transfer you want to perform, the public IP from where the connection will be made and the time duration (approximate) in order to grant you access to that IP .


- **Access to the DTN nodes for external users:**
We refer to external users to those who are not registered in our services. The transfer of data between CESGA users and external is possible, also for uploading and downloading data from both parts. A possible scenario for this exchange would be sharing data with external collaborators of our users. If this is your case, the protocol to grant this access is:

By the CESGA user, contact the Systems Department, explaining that you need to transfer data (or receive data) from an external collaborator. If the transfer is from CESGA user to the external one, it must be indicated the directory where the data is located. The external user will have the credentials to access the DTN nodes through user + pass. In this case, the connection is only allowed by ``sftp`` command. 

You must also provide the public IP of the external user to grant the access (because as previously mentioned, these nodes cannot be accessed via VPN and the IPs must be included in our system). You must also indicate the approximate time in which the service will be offered for the data transferring to be performed. 


Migrate LUSTRE data from FinisTerrae II to FinisTerrae III
----------------------------------------------------------
To transfer data between both machines you can use commands like ``scp`` or ``rsync``. You can use commands like the following examples:

.. code-block:: console

        $ scp -rp $LUSTRE/* ft3.cesga.es:PATH_LUSTRE_FT3/

        $ rsync -avHAP $LUSTRE/ ft3.cesga.es:PATH_LUSTRE_FT3/


To obtain the value of *PATH_LUSTRE_FT3* you can execute the command ``echo $LUSTRE**`` in FinisTerrae III.

In case of interruption, running the ``rsync`` command will resume the transfer from the point where it was cut off. The recommendation is to run it several times to ensure that no files remain unsynchronized.

It could be recommended to use the command ``screen`` to execute these commands to transfer these data, because it allows you to resume the sessions. Screen is a terminal multiplexer. In other words, it means that you can start a screen session and then open any number of windows (virtual terminals) inside that session. Processes running in Screen will continue to run when their window is not visible even if you get disconnected. If you use this command, you have to remember the login node where you have launched it, to be able to resume your screen session.
