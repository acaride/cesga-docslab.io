.. CESGA documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

####################################
  CESGA Technical Documentation
####################################
.. toctree::

   ft2-user-guide/index

.. toctree::

   ft3-user-guide/index

.. toctree::

   cloud-openstack/index

.. toctree::

   dtn-user-guide/index

.. toctree::

   bigdata-user-guide/index

.. toctree::

   qrng/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
