.. QRNG User Guide documentation master file

Quantum Random Number Generator (QRNG)
======================================

The Quantum Random Number Generator (QRNG) is a device that was integrated into the computing and communications infrastructure of CESGA in November 2021. The update to version 2.0.0 was implemented in January 2023, which reduces the data processing requirements of Python, improving the performance of user applications. The device installed is the FMC 400 model from the company `Quside. <https://quside.com/quantum-random-number-generators-why-how-where/>`_ 

This QRNG allows for the generation of random binary sequences with the following characteristics:

- Typical entropy of at least 0.94.
- Unprocessed randomness rate of 400Mb/s.
- Minimum random bit output rate of 100Mb/s.
- There are no limitations on the maximum number of random numbers that can be requested now. However, the host machine's memory imposes limitations.

The system has a waiting time to manage the connection of an inactive client via Ethernet (default of 300 seconds).

The system is installed in the CESGA data center, connected to the Ethernet network and currently has a **restriction on connected users**, being able to serve only **one client** simultaneously. The equipment has APIs for access from Python and C-based clients. To facilitate access, these APIs are available to users who request them by contacting the applications department, allowing their use by internal and external researchers.

Use cases
---------

This equipment is designed to explore and investigate applications that require or use random numbers. Among the identified areas that require this type of solution are:

- Cybersecurity solutions.
- Cryptography and electronic signature solutions.
- Scientific and technical computing solutions that use randomness:
- Monte Carlo algorithms, optimization, etc.
- Solutions that require reliable and unpredictable random order, such as sorting lists.
- Solutions that require reliable and unpredictable random selection.

There may be other use cases that will be identified throughout its lifespan.

Management
----------

The equipment is managed following the policies and procedures applied in CESGA for computing and storage infrastructure equipment. At this time, it is an isolated device for experimentation in the previously mentioned fields.

Access protocol
---------------

This equipment is intended for use by any public or private researcher who wishes to experiment, conduct proof of concept or demonstrate use cases of the technology. Given its experimental nature, **access to the machine requires submitting a usage request** including a small technical report describing the research, development or innovation must be submitted. The report must include:

- Entity and/or responsible researcher.
- Brief description of the project objective.
- Expected duration of access.
- Requested initial date for access.

This report must be sent to **aplicaciones@cesga.es** in order to request the access to QRNG.

Access allocation
-----------------

Due to the current system limitation, the total duration of an access cannot exceed three months, continuously or not.

In the allocation of access, priority will be given to requests from applicants affiliated with entities with workplaces in Galicia. To access this system, the applicant must have a valid CESGA user account. If the applicant does not have a user account, they must request one following the usual CESGA procedures.

Once the user is registered, we will send them a guide on how to connect and use the equipment.

Final report
--------------

In case access is granted, the applicant must submit a final report with the obtained results upon completion.

Dissemination and publicity
---------------------------

The applicant who has been granted access to the system, in case of dissemination or publication of the obtained results or in the licenses of products or services generated from the experiences, pilots or demonstrators, **must indicate that these results were obtained thanks to the use of our infrastructure**. Specifically, the acknowledgment (in Galician, Spanish, or English) must appear as follows:

    “Work developed thanks to the access granted by the Galician Supercomputing Center to the quantum information-based infrastructure that allows to boost R&D&I in Galicia. This infrastructure was financed by the European Union through the EUROPEAN REGIONAL DEVELOPMENT FUND (ERDF) as part of the Union's response to the COVID-19 pandemic."

Galician version:

    “Traballo desenvolvido gracias ao acceso concedido polo Centro de Supercomputación de Galicia á infraestrutura baseada de tecnoloxías cuánticas da información que permita impulsar a I+D+I en Galicia. Esta infraestrutura foi financiada pola Unión Europa, a través do FONDO EUROPEO DE DESENVOLVEMENTO REXIONAL (FEDER), como parte da resposta da Unión á pandemia da COVID-19.”

Spanish version:

    "Trabajo desarrollado gracias al acceso concedido por el Centro de Supercomputación de Galicia a la infraestructura basada de tecnologías cuánticas de la información que permita impulsar I+D+I en Galicia. Esta infraestructura gue financaida por la Unión Europea, a través del FONDO EUROPEO DE DESARROLLO REGIONAL (FEDER), como parte de la respuesta de la Unión Europa a la pandemia de la COVID-19."

Usage fees
-----------

Given its experimental nature, during the years 2021, 2022 and 2023, access to this infrastructure will be **free** of charge for the applicant.
