.. CESGA Hadoop 3 User's Guide documentation master file, created by
   sphinx-quickstart on Thu Apr  4 20:52:55 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BD|CESGA Hadoop 3 User Guide
============================

.. toctree::

   overview
   quickstart
   whats_new
   known_issues
   how_to_connect
   webui
   hue
   migrating_data
   quota
   how_to_upload_data
   yarn
   hdfs
   spark
   sparklyr
   jupyter
   hive
   impala
   sqoop
   modules
   want_to_know_more
   vpn
