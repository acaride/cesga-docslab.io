.. _want_to_know_more:

Want to know more
=================

For more information we recommend that you start looking at the tutorials that we have prepared to get you started with each of the tools.

After that, if you need additional information you can check CDH documentations with the different component guides as well as the official documentation of each component.

Tutorials
---------

You can find the complete list of tutorials at https://bigdata.cesga.es/#tutorials

- `HDFS Tutorial`_
- `YARN Tutorial`_
- `Spark Tutorial`_
- `Jupyter Tutorial`_
- `Hive Tutorial`_
- `Sqoop Tutorial`_
- `MapReduce Tutorial`_

Additionally you can find useful the material of the PySpark and Sparklyr courses:

- `PySpark Course Material`_
- `Sparklyr Course Material`_

CDH Documentation
-----------------

- `CDH Documentation`_ for CDH 6.1
- `Spark Guide`_
- `Hive Guide`_
- `Impala Guide`_
- `HUE Guide`_

Reference Documentation
-----------------------

In the following page you can find the links to the `Reference documentation for each component`_.


.. _HDFS Tutorial: https://bigdata.cesga.es/tutorials/hdfs.html
.. _YARN Tutorial: https://bigdata.cesga.es/tutorials/yarn.html
.. _Spark Tutorial: https://bigdata.cesga.es/tutorials/spark.html
.. _Jupyter Tutorial: https://bigdata.cesga.es/tutorials/hdfs.html
.. _Hive Tutorial: https://bigdata.cesga.es/tutorials/hive.html
.. _Sqoop Tutorial: https://bigdata.cesga.es/tutorials/hdfs.html
.. _MapReduce Tutorial: https://bigdata.cesga.es/tutorials/hdfs.html

.. _PySpark Course Material: https://github.com/javicacheiro/pyspark_course
.. _Sparklyr Course Material: https://github.com/aurora-mareviv/sparklyr_test

.. _CDH Documentation: //www.cloudera.com/documentation/enterprise/6/6.1/topics/cdh_components.html
.. _Impala Guide: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/impala.html
.. _Hive Guide: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/hive.html
.. _HUE Guide: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/hue.html
.. _Spark Guide: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/spark.html

.. _`Reference documentation for each component`:  https://www.cloudera.com/documentation/enterprise/6/6.1/topics/cdh_component_documentation_links.html
