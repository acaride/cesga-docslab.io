.. _webui:

BD|CESGA WebUI
==============

The cluster has a Web User Interface that can be accessed through: https://bigdata.cesga.es

From the main BD|CESGA page you can access the WebUI as well as find additional information about the platform including several tutorials that will help you to start using it.

.. figure:: _static/screenshots/main.png
    :align: center

    The main BD|CESGA page.


The BD|CESGA Web User Interface provides you access to useful web interfaces that will allow you to use the platform in a graphical way.

.. figure:: _static/screenshots/webui.png
    :align: center

    The BD|CESGA Web User Interface.
